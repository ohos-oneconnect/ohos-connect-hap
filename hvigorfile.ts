// Script for compiling build behavior. It is built in the build plug-in and cannot be modified currently.
import { appTasks } from '@ohos/hvigor-ohos-plugin';

export default {
  system: appTasks,
  plugins:[]
}