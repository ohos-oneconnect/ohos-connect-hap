/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import DeviceDiscoverManager from '../viewmodel/DeviceDiscoverManager'

/**
 * 重新扫描按钮
 */
@Component
export struct RescanButton {
  build() {
    Button(
      $r('app.string.scan_no_result_button_text'),
      { type: ButtonType.Capsule, stateEffect: true }
    )
      .onClick(() => {
        DeviceDiscoverManager.getInstance().startScan();
      })
      .fontColor($r('app.color.button_text_color'))
      .fontSize($r('app.float.button_font_size'))
      .padding({
        left: $r('app.float.button_padding_horizontal'),
        right: $r('app.float.button_padding_horizontal'),
        top: $r('app.float.button_padding_vertical'),
        bottom: $r('app.float.button_padding_vertical')
      })
      .backgroundColor($r('app.color.scan_button_color'))
  }
}