/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DetailItemInfo } from '../../viewmodel/DeviceLibraryManager'
import { SearchResultsItemComponent } from './SearchResultsItemComponent'

@Component
export struct SearchResultsComponent {
  @Consume currentSearchField: string
  @Consume searchResultList: Array<DetailItemInfo>

  build() {
    Column() {
      if (this.searchResultList.length == 0) {
        Text($r('app.string.no_results_contain_str', this.currentSearchField)).textAlign(TextAlign.Start).width('100%')
      } else {
        Text($r('app.string.results_include_str', this.currentSearchField)).textAlign(TextAlign.Start).width('100%')
        List() {
          ForEach(this.searchResultList, (data: DetailItemInfo) => {
            ListItem() {
              SearchResultsItemComponent({ searchInfo: data })
            }.width('100%')
          })
        }
        .divider(new DividerTmp(1, 10, 10, '#ffe9f0f0'))
        .backgroundColor(Color.White)
        .borderRadius(20)
        .margin({ top: 10, bottom: 35 })
      }
    }.height('100%').margin({ left: 20, right: 20, top: 20 })
  }
}

class DividerTmp {
  strokeWidth: Length = 1
  startMargin: Length = 10
  endMargin: Length = 10
  color: ResourceColor = '#ffe9f0f0'

  constructor(strokeWidth: Length, startMargin: Length, endMargin: Length, color: ResourceColor) {
    this.strokeWidth = strokeWidth
    this.startMargin = startMargin
    this.endMargin = endMargin
    this.color = color
  }
}