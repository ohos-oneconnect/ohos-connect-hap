import { promptAction } from '@kit.ArkUI';
import { EventActionUIInfoDataItem, IUiInfo } from '../../model/SceneCondition';

@Component
struct TitleHeader {
  @Prop title: string
  cancel?: () => void
  confirm?: () => void

  build() {
    Row() {
      Text($r('app.string.button_text_cancel')).fontColor('#0A59F7').fontSize($r('app.float.font_14')).onClick(() => this.cancel && this.cancel())
      Text('选择' + this.title).fontColor('#000000').fontSize($r('app.float.font_16'))
      Text($r('app.string.button_text_confirm')).fontColor('#0A59F7').fontSize($r('app.float.font_14')).onClick(() => this.confirm && this.confirm())
    }
    .justifyContent(FlexAlign.SpaceBetween)
    .width('100%')
    .height($r('app.float.scene_tab_height'))
    .padding({ left: $r('app.float.p_24'), right: $r('app.float.p_24') })
  }
}


@Component
export struct modifySceneAction {
  @Link data: EventActionUIInfoDataItem
  cancel?: () => void
  confirm?: () => void
  @State actionVal: string = ''

  build() {
    Column() {
      TitleHeader({
        title: this.data.name || '',
        cancel: () => this.cancel && this.cancel(),
        confirm: () => {

          if (this.actionVal) {
            switch (this.data.type) {
              case 'enum':
                let data = this.data.uiInfo.find((res) => res.enumVal === this.actionVal)
                this.data.selectedName = data?.descCh || ''
                this.data.selectedValue = data?.enumVal || ''
                break;
              case 'int':
                this.data.selectedName = this.actionVal + this.data.unit || ''
                this.data.selectedValue = this.actionVal || ''
                break;
              default:
                break;
            }
            this.confirm && this.confirm()
          } else {
            // promptAction.showToast({
            //   message: '请选择' + this.data.name,
            //   duration: 2000
            // });
          }
        },
      })

      Column() {
        Flex({ justifyContent: FlexAlign.Center }) {
          if (this.data.type == 'enum') {
            GridRow({
              columns: 3,
              gutter: { x: 5, y: 10 },
              breakpoints: {
                value: ["400vp", "600vp", "800vp"],
                reference: BreakpointsReference.WindowSize
              },
              direction: GridRowDirection.Row
            }) {
              ForEach(this.data.uiInfo, (item: IUiInfo) => {
                GridCol() {
                  Row({ space: 8 }) {
                    Text(item.descCh)
                      .fontColor('#333333')
                      .fontSize('16fp')
                      .textOverflow({ overflow: TextOverflow.Ellipsis })
                      .maxLines(1)
                      .width('50vp')
                    Radio({ value: 'Radio1', group: 'radioGroup', indicatorType: RadioIndicatorType.TICK })
                      .checked(false)
                      .height('24vp')
                      .width('24vp')
                      .onChange((isChecked: boolean) => {
                        if (isChecked) {
                          this.actionVal = item.enumVal
                          console.log('Radio1 status is2 ' + JSON.stringify(this.actionVal))
                        }
                      })
                  }
                }
              })
            }.width("100%").height("100%")
          } else if (this.data.type == 'int') {
            Row({ space: 8 }) {
              Slider({
                value: Number(this.actionVal),
                min: Number(this.data.min) || 0,
                max: Number(this.data.max) || 0,
                style: SliderStyle.NONE
              })
                .blockColor('#191970')
                .trackColor('#ADD8E6')
                .selectedColor('#4169E1')
                .onChange((value: number, mode: SliderChangeMode) => {
                  if (Number(this.data.min) <= value && Number(this.data.max) >= value) {
                    this.actionVal = `${value}`
                    console.info('value:' + this.actionVal + 'mode:' + mode.toString())
                  }
                })
                .width('90%')

              Text(Number(this.actionVal).toFixed(0) + this.data.unit ?? '').fontSize(12)
            }.width('100%')
          }
        }

      }.padding($r('app.float.p_24'))
    }.height('100%').width('100%')
  }
}