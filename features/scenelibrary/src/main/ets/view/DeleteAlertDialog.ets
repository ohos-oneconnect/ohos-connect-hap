/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonConstants } from '@ohos/common/src/main/ets/constants/CommonConstants'

@CustomDialog
export default struct DeleteAlertDialog {
  onConfirm: Function = () => {
  };
  controller: CustomDialogController = new CustomDialogController({
    builder: this
  });

  build() {
    Column() {
      Text($r('app.string.delete_scene_tip'))
        .fontSize($r('app.float.font_16'))
        .margin({ bottom: $r('app.float.dialog_padding_bottom') })
        .fontWeight(FontWeight.Medium)
        .width(CommonConstants.FULL_WIDTH_PERCENT)

      Row() {
        Text($r('app.string.button_text_cancel'))
          .fontColor($r('app.color.button_text_color'))
          .fontSize($r('app.float.font_16'))
          .fontWeight(FontWeight.Medium)
          .onClick(() => {
            this.controller.close();
          })

        Divider().vertical(true)
          .color($r('app.color.dialog_divider_color'))
          .height($r('app.float.dialog_divider_height'))

        Text($r('app.string.button_text_confirm'))
          .fontColor($r('app.color.button_text_confirm_color'))
          .fontSize($r('app.float.font_16'))
          .fontWeight(FontWeight.Medium)
          .onClick(() => {
            this.onConfirm();
          })
      }.justifyContent(FlexAlign.SpaceAround)
      .width(CommonConstants.FULL_WIDTH_PERCENT)
    }.padding($r('app.float.dialog_padding_bottom'))
  }
}