/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HMRouter } from '@hadss/hmrouter'
import { NavBar } from '@ohos/common'
import { LanguageComponentInfo, LanguageItemInfo } from '../../viewmodel/LanguageViewModel'

/**
 * 设置页面-语言项组件
 */
@Component
struct LanguageItemComponent {
  @ObjectLink itemInfo: LanguageItemInfo

  build() {
    Row() {
      Text(this.itemInfo.language.languageDisplay)
      Blank()
      if (this.itemInfo.isSelected) {
        Image($r('app.media.ic_public_ok')).height(15)
      }
    }.width('100%').padding(10)
  }
}

/**
 * 语言页面UI组件
 */
@HMRouter({ pageUrl: 'LanguagePage' })
@Component
export struct LanguagePage {
  @State info: LanguageComponentInfo | undefined = undefined

  aboutToAppear(): void {
    LanguageComponentInfo.getInstance().then((data) => {
      this.info = data
    })
  }

  showFunc(item: LanguageItemInfo, isShow: boolean) { //切换语言动效展示
    for (let newItem of LanguageComponentInfo.newSelectedLanguageList) {
      newItem.showFunc(false)
    }
    item.showFunc(isShow)
    LanguageComponentInfo.newSelectedLanguageList = []
    if (isShow) {
      LanguageComponentInfo.newSelectedLanguageList.push(item)
    }
    for (let old of LanguageComponentInfo.oldSelectedLanguageList) {
      old.showFunc(!isShow)
    }
  }

  handleClick(item: LanguageItemInfo) { //点击语言某项时执行动作
    this.showFunc(item, !item.isSelected)
  }

  build() {
    Column() {
      NavBar({
        title: $r('app.string.page_settings_language'), rightText: $r('app.string.sure'), clickCallback: () => {
          // 点击确认切换应用偏好语言
          if (LanguageComponentInfo.newSelectedLanguageList.length == 1 &&
            LanguageComponentInfo.newSelectedLanguageList[0] != undefined &&
            LanguageComponentInfo.newSelectedLanguageList[0].language != undefined) {
            LanguageComponentInfo.setAppLanguage(LanguageComponentInfo.newSelectedLanguageList[0].language)
          }
        }
      })
      Column() { //数据库中已选择的语言，第一次设置之前为默认的中文
        if (this.info != undefined) {
          List() {
            ForEach(LanguageComponentInfo.oldSelectedLanguageList, (item: LanguageItemInfo) => {
              ListItem() {
                LanguageItemComponent({ itemInfo: item })
              }
            })
          }

          Divider().margin({ left: 5, right: 5 })
          List() { //可选语言列表
            ForEach(LanguageComponentInfo.languageList, (item: LanguageItemInfo) => {
              ListItem() {
                LanguageItemComponent({ itemInfo: item }).onClick(() => {
                  this.handleClick(item)
                })
              }
            })
          }
        }
      }.backgroundColor(Color.White).borderRadius(15).margin({ left: 10, right: 10 }).padding(5)
    }.width('100%')
    .height('100%')
    .backgroundColor($r("sys.color.ohos_id_color_sub_background"))
    .expandSafeArea([SafeAreaType.SYSTEM], [SafeAreaEdge.TOP, SafeAreaEdge.BOTTOM]);
  }
}
